<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkAdmin
// | github 代码仓库：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\index\controller;

use app\wechat\service\WechatService;
use think\admin\Controller;
use think\admin\extend\CodeExtend;
use think\facade\Db;
use think\Request;

class Index extends Controller
{
    public function index()
    {
        
    }

    // public function index(Request $request)
    // {
    //     $id = $request->get('id', '');
    //     if (!$id) {
    //         $this->error('参数错误');
    //     }

    //     $school = Db::table('jk_school')->where('url', $id)->find();
    //     if (!$school) {
    //         $this->error('没有找到对应的学校');
    //     }
    //     $product = Db::table('jk_product')->where('school_id', $school['id'])->select();
    //     if (!$product) {
    //         $this->error('没有找到校服数据');
    //     }

    //     $this->assign('title', $this->getTitle($school['name']));
    //     $this->assign('school', $school);
    //     $this->assign('product', $product);
    //     $this->fetch('index');
    // }

    // public function shop(Request $request)
    // {
    //     $id = $request->get('id', '');
    //     if (!$id) {
    //         $this->error('参数错误');
    //     }

    //     $product = Db::table('jk_product')->where('url', $id)->find();
    //     if (!$product) {
    //         $this->error('没有找到校服数据');
    //     }
    //     $school = Db::table('jk_school')->where('id', $product['school_id'])->find();
    //     if (!$product) {
    //         $this->error('没有找到学校数据');
    //     }

    //     $grade = Db::table('jk_grade')->where('school_id', $product['school_id'])->select();

    //     $openid = '';
    //     if ($this->isWechat($request)) {
    //         $this->url = $this->request->url(true);
    //         $user = WechatService::instance()->getWebOauthInfo($this->url, 0);
    //         if (empty($user['openid'])) {
    //             return $this->error('网页授权获取OPENID失败！');
    //         }
    //         $openid = $user['openid'];
    //     }

    //     $this->assign('title', '订购信息填写 - ' . $this->getTitle($school['name']));
    //     $this->assign('school', $school);
    //     $this->assign('product', $product);
    //     $this->assign('grade', $grade);
    //     $this->assign('openid', $openid);
    //     $this->fetch('shop');
    // }

    // public function getClassList(Request $request)
    // {
    //     $gradeId = $request->get('id', '');
    //     if (!$gradeId) {
    //         return $this->error('参数错误');
    //     }
    //     $classList = Db::table('jk_class')->field('id,name')->where('grade_id', $gradeId)->select();
    //     if (!$classList) {
    //         return $this->error('没有找到对应的班级');
    //     }
    //     return $this->success('获取成功', $classList);
    // }

    // public function order(Request $request)
    // {
    //     $param = $request->param();
    //     $product = Db::table('jk_product')->where('id', $param['product_id'])->find();
    //     if (!$product) {
    //         return $this->error('没有找到校服数据');
    //     }
    //     $param['order_no'] = CodeExtend::uniqidNumber(32);
    //     $param['order_price'] = number_format(intval($param['order_num']) * $product['price'], 2);
    //     $param['create_time'] = time();
    //     $isSave = true;
    //     $isSave = Db::table('jk_order')->save($param);
    //     $userAgent = 'pc';
    //     $this->pay = WechatService::WePayOrder();
    //     $data = [];
    //     if ($this->isWechat($request)) {
    //         $userAgent = 'wechat';
    //         $result = $this->pay->create([
    //             'body'             => $product['name'] . ' / 数量：' . $param['order_num'],
    //             'openid'           => $param['open_id'],
    //             'total_fee'        => $param['order_price'] * 100,
    //             'trade_type'       => 'JSAPI',
    //             'notify_url'       => sysuri('wechat/api.test/notify', [], false, true),
    //             'out_trade_no'     => $param['order_no'],
    //             'spbill_create_ip' => $this->request->ip(),
    //         ]);
    //         // var_dump($result);
    //         if ($result['return_code'] == 'SUCCESS') {
    //             // 创建JSAPI参数签名
    //             $options = $this->pay->jsapiParams($result['prepay_id']);
    //             // JSSDK 签名配置
    //             $optionJSON = json_encode($options, JSON_UNESCAPED_UNICODE);
    //             $configJSON = json_encode(WechatService::instance()->getWebJssdkSign(), JSON_UNESCAPED_UNICODE);
    //             $data['optionJSON'] = $optionJSON;
    //             $data['configJSON'] = $configJSON;
    //         } else {
    //             return $this->error('微信支付出错');
    //         }
    //     } else {
            
    //     }
    //     $data['userAgent'] = $userAgent;
    //     if ($isSave) {
    //         return $this->success('下单成功', $data);
    //     } else {
    //         return $this->error('下单出错');
    //     }
    // }

    // protected function getTitle($schoolName)
    // {
    //     return '金彩校服运营中心'. $schoolName .'学校服装订购中心';
    // }

    // protected function isWechat(Request $request)
    // {
    //     $httpUserAgent = $request->server('HTTP_USER_AGENT');
    //     if (strpos($httpUserAgent, 'MicroMessenger') !== false) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}