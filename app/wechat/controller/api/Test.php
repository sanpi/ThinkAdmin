<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkAdmin
// | github 代码仓库：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\wechat\controller\api;

use app\wechat\service\WechatService;
use think\admin\Controller;
use think\admin\extend\CodeExtend;
use think\facade\Db;
use think\Request;
use WeChat\Contracts\Tools;

/**
 * 微信测试工具
 * Class Test
 * @package app\wechat\controller\api
 */
class Test extends Controller
{
    public function index(Request $request)
    {
        $id = $request->get('id', '');
        if (!$id) {
            $this->error('参数错误');
        }

        $school = Db::table('jk_school')->where('url', $id)->find();
        if (!$school) {
            $this->error('没有找到对应的学校');
        }
        $product = Db::table('jk_product')->where('school_id', $school['id'])->order('sort desc, id asc')->select();
        if (!$product) {
            $this->error('没有找到校服数据');
        }

        $this->assign('title', $this->getTitle($school['name']));
        $this->assign('school', $school);
        $this->assign('product', $product);
        $this->fetch('api/index/index');
    }

    public function shop(Request $request)
    {
        $id = $request->get('id', '');
        if (!$id) {
            $this->error('参数错误');
        }

        $product = Db::table('jk_product')->where('url', $id)->find();
        if (!$product) {
            $this->error('没有找到校服数据');
        }
        $school = Db::table('jk_school')->where('id', $product['school_id'])->find();
        if (!$product) {
            $this->error('没有找到学校数据');
        }

        $grade = Db::table('jk_grade')->where('school_id', $product['school_id'])->select();

        $openid = '';
        $userAgent = 'pc';
        if ($this->isWechat($request)) {
            $userAgent = 'wechat';
            $this->url = $this->request->url(true);
            $user = WechatService::instance()->getWebOauthInfo($this->url, 0);
            if (empty($user['openid'])) {
                return $this->error('网页授权获取OPENID失败！');
            }
            $openid = $user['openid'];
        } else if ($request->isMobile()) {
            $userAgent = 'mobile';
        }

        $this->assign('title', '订购信息填写 - ' . $this->getTitle($school['name']));
        $this->assign('school', $school);
        $this->assign('product', $product);
        $this->assign('grade', $grade);
        $this->assign('openid', $openid);
        $this->assign('userAgent', $userAgent);
        $this->fetch('api/index/shop');
    }

    public function getClassList(Request $request)
    {
        $gradeId = $request->get('id', '');
        if (!$gradeId) {
            return $this->error('参数错误');
        }
        $classList = Db::table('jk_class')->field('id,name')->where('grade_id', $gradeId)->select();
        if (!$classList) {
            return $this->error('没有找到对应的班级');
        }
        return $this->success('获取成功', $classList);
    }

    public function order(Request $request)
    {
        $param = $request->param();
        $product = Db::table('jk_product')->where('id', $param['product_id'])->find();
        if (!$product) {
            return $this->error('没有找到校服数据');
        }
        $orderNo = '';
        while (true) {
            $orderNo = CodeExtend::uniqidNumber(32);
            $orderResult = Db::table('jk_order')->field('id,order_no')->where('order_no', $orderNo)->find();
            if (!$orderResult) {
                break;
            }
        }

        if (intval($param['order_num']) <= 0) {
            return $this->error('订购数量必须大于0');
        }

        $param['order_no'] = $orderNo;
        $param['order_price'] = number_format(intval($param['order_num']) * $product['price'], 2);
        $param['create_time'] = time();

        $isSave = true;
        $isSave = Db::table('jk_order')->save($param);
        $this->pay = WechatService::WePayOrder();

        $userAgent = 'pc';
        $data = [];
        if ($this->isWechat($request)) {
            $userAgent = 'wechat';
            $result = $this->pay->create([
                'body'             => $product['name'] . ' / 数量：' . $param['order_num'],
                'openid'           => $param['open_id'],
                'total_fee'        => $param['order_price'] * 100,
                'trade_type'       => 'JSAPI',
                'notify_url'       => sysuri('wechat/api.test/notify', [], false, true),
                'out_trade_no'     => $param['order_no'],
                'spbill_create_ip' => $this->getClientIp(),
            ]);
            
            if ($result['return_code'] == 'SUCCESS') {
                // 创建JSAPI参数签名
                $options = $this->pay->jsapiParams($result['prepay_id']);
                // JSSDK 签名配置
                $optionJSON = json_encode($options, JSON_UNESCAPED_UNICODE);
                $configJSON = json_encode(WechatService::instance()->getWebJssdkSign(), JSON_UNESCAPED_UNICODE);
                $data['optionJSON'] = $optionJSON;
                $data['configJSON'] = $configJSON;
            } else {
                return $this->error('微信支付出错');
            }
        } else if ($request->isMobile()) {
            $userAgent = 'mobile';
            $result = $this->pay->create([
                'body'             => $product['name'] . ' / 数量：' . $param['order_num'],
                'total_fee'        => $param['order_price'] * 100,
                'trade_type'       => 'MWEB',
                'notify_url'       => sysuri('wechat/api.test/notify', [], false, true),
                'out_trade_no'     => $param['order_no'],
                'spbill_create_ip' => $this->getClientIp(),
            ]);
            
            if ($result['return_code'] == 'SUCCESS') {
                $data['mweb_url'] = $result['mweb_url'] . '&redirect_url=' . urlencode(url('order_result', [], true, true) . '?order=' . $param['order_no']);
            } else {
                return $this->error('微信支付出错');
            }
        }
        $data['userAgent'] = $userAgent;
        $data['order_no'] = $param['order_no'];
        if ($isSave) {
            return $this->success('下单成功', $data);
        } else {
            return $this->error('下单出错');
        }
    }

    protected function getClientIp($type = 0) {
        $type       =  $type ? 1 : 0;
        static $ip  =   NULL;
        if ($ip !== NULL) return $ip[$type];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos    =   array_search('unknown',$arr);
            if(false !== $pos) unset($arr[$pos]);
            $ip     =   trim($arr[0]);
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip     =   $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u",ip2long($ip));
        $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }    

    protected function getTitle($schoolName)
    {
        return '金彩校服运营中心'. $schoolName .'学校服装订购中心';
    }

    protected function isWechat(Request $request)
    {
        $httpUserAgent = $request->server('HTTP_USER_AGENT');
        if (strpos($httpUserAgent, 'MicroMessenger') !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function notice()
    {
        $this->assign('title', '订购须知');
        $this->fetch('api/index/notice');
    }

    public function order_result(Request $request)
    {
        $orderNo = $request->get('order', '');
        if (!$orderNo) {
            $this->error('错误的参数');
        }
        $wechat = WechatService::WePayOrder();
        $queryResult = $wechat->query([
            'out_trade_no' => $orderNo
        ]);
        $icon = 'success';
        $msg = $queryResult['trade_state_desc'];
        // 支付成功
        if ($queryResult['trade_state'] == 'SUCCESS') {
            $orderResult = Db::table('jk_order')->where('order_no', $orderNo)->find();
            if (!$orderResult) {
                $icon = 'warn';
                $msg = '订单不存在';
            } else {
                if ($orderResult['pay_status'] == 0) {
                    Db::table('jk_order')->where('order_no', $orderNo)->update([
                        'pay_status' => 1,
                        'transaction_id' => $orderResult['transaction_id'],
                        'open_id' => $orderResult['openid'],
                    ]);
                }
            }
        } else {
            $icon = 'warn';
        }
        $this->assign('icon', $icon);
        $this->assign('msg', $msg);
        $this->assign('isWechat', $this->isWechat($request));
        $this->assign('title', '支付结果');
        $this->fetch('api/index/result');
    }

    public function order_result_ajax(Request $request)
    {
        $orderNo = $request->get('order', '');
        if (!$orderNo) {
            $this->error('错误的参数');
        }
        $wechat = WechatService::WePayOrder();
        $queryResult = $wechat->query([
            'out_trade_no' => $orderNo
        ]);
        
        // 支付成功
        if ($queryResult['trade_state'] == 'SUCCESS') {
            $orderResult = Db::table('jk_order')->where('order_no', $orderNo)->find();
            if (!$orderResult) {
                $this->error('订单不存在');
            }
            $this->success('支付成功');
        } else {
            $this->error('订单支付失败');
        }
    }

    public function order_qrcode_scan(Request $request)
    {
        $orderNo = $request->get('order', '');
        if (!$orderNo) {
            $this->error('错误的参数');
        }

        $this->assign('orderNo', $orderNo);
        $this->assign('title', '扫码支付');
        $this->fetch('api/index/qrcode');
    }

    // 支付二维码
    public function order_qrcode(Request $request)
    {
        $orderNo = $request->get('order', '');
        if (!$orderNo) {
            $this->error('错误的参数');
        }
        $orderResult = Db::table('jk_order')->where('order_no', $orderNo)->find();
        if (!$orderResult) {
            $this->error('订单不存在');
        }
        $product = Db::table('jk_product')->where('id', $orderResult['product_id'])->find();
        if (!$product) {
            $this->error('产品不存在');
        }
        $orderPrice = number_format(intval($orderResult['order_num']) * $product['price'], 2);

        $result = WechatService::WePayOrder()->create([
            'body'             => $product['name'] . ' / 数量：' . $orderResult['order_num'],
            'total_fee'        => $orderPrice * 100,
            'trade_type'       => 'NATIVE',
            'notify_url'       => sysuri('wechat/api.test/notify', [], false, true),
            'out_trade_no'     => $orderNo,
            'spbill_create_ip' => $this->getClientIp(),
        ]);
        return $this->showQrc($result['code_url']);
    }

    /**
     * 支付通知接收处理
     * @return string
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function notify()
    {
        $wechat = WechatService::WePayOrder();
        $result = $wechat->getNotify();
        if ($result['result_code'] == 'SUCCESS') {
            Db::table('jk_order')->where('order_no', $result['out_trade_no'])->update([
                'pay_status' => 1,
                'open_id' => $result['openid'],
                'transaction_id' => $result['transaction_id']
            ]);
            return 'SUCCESS';            
        } else {
            return 'FAIL';
        }
    }

    /**
     * 微信JSAPI支付二维码
     * @return \think\Response
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionFailedException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     * @throws \Endroid\QrCode\Exceptions\ImageTypeInvalidException
     */
    public function jsapi_qrc()
    {
        $this->url = sysuri('wechat/api.test/jsapi', [], false, true);
        return $this->showQrc($this->url);
    }

    /**
     * 显示网页授权二维码
     * @return \think\Response
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionFailedException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     * @throws \Endroid\QrCode\Exceptions\ImageTypeInvalidException
     */
    public function oauth_qrc()
    {
        $this->url = sysuri('wechat/api.test/oauth', [], false, true);
        return $this->showQrc($this->url);
    }

    /**
     * 显示网页授权二维码
     * @return \think\Response
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionFailedException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     * @throws \Endroid\QrCode\Exceptions\ImageTypeInvalidException
     */
    public function jssdk_qrc()
    {
        $this->url = sysuri('wechat/api.test/jssdk', [], false, true);
        return $this->showQrc($this->url);
    }

    /**
     * 微信扫码支付模式一二维码显示
     * @return \think\Response
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionFailedException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     * @throws \Endroid\QrCode\Exceptions\ImageTypeInvalidException
     */
    public function scan_one_qrc()
    {
        $pay = WechatService::WePayOrder();
        return $this->showQrc($pay->qrcParams('8888888'));
    }

    /**
     * 扫码支付模式二测试二维码
     * @return \think\Response
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionFailedException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     * @throws \Endroid\QrCode\Exceptions\ImageTypeInvalidException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function scan_two_qrc()
    {
        $result = WechatService::WePayOrder()->create([
            'body'             => '测试商品',
            'total_fee'        => '1',
            'trade_type'       => 'NATIVE',
            'notify_url'       => sysuri('wechat/api.test/notify', [], false, true),
            'out_trade_no'     => CodeExtend::uniqidNumber(18),
            'spbill_create_ip' => $this->getClientIp(),
        ]);
        return $this->showQrc($result['code_url']);
    }

    /**
     * 网页授权测试
     * @return string
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function oauth()
    {
        $this->url = $this->request->url(true);
        $this->fans = WechatService::instance()->getWebOauthInfo($this->url, 1);
        $this->fetch();
    }

    /**
     * JSSDK测试
     * @return string
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function jssdk()
    {
        $this->options = WechatService::instance()->getWebJssdkSign();
        $this->fetch();
    }

    /**
     * 微信扫码支付模式一通知处理
     * -- 注意，需要在微信商户配置支付通知地址
     * @return string
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function scan_one_notify()
    {
        $pay = WechatService::WePayOrder();
        $notify = $pay->getNotify();
        p('======= 来自扫码支付1的数据 ======');
        p($notify);
        // 微信统一下单处理
        $options = [
            'body'             => "测试商品，产品ID：{$notify['product_id']}",
            'total_fee'        => '1',
            'trade_type'       => 'NATIVE',
            'notify_url'       => sysuri('wechat/api.test/notify', [], false, true),
            'out_trade_no'     => CodeExtend::uniqidDate(18),
            'spbill_create_ip' => $this->getClientIp(),
        ];
        $order = $pay->create($options);
        p('======= 来自扫码支付1统一下单结果 ======');
        p($order);
        // 回复XML文本
        $result = [
            'return_code' => 'SUCCESS',
            'return_msg'  => '处理成功',
            'appid'       => $notify['appid'],
            'mch_id'      => $notify['mch_id'],
            'nonce_str'   => Tools::createNoncestr(),
            'prepay_id'   => $order['prepay_id'],
            'result_code' => 'SUCCESS',
        ];
        $result['sign'] = $pay->getPaySign($result);
        p('======= 来自扫码支付1返回的结果 ======');
        p($result);
        return Tools::arr2xml($result);
    }

    /**
     * 微信JSAPI支付测试
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @link wx-demo-jsapi
     */
    public function jsapi()
    {
        $this->url = $this->request->url(true);
        $this->pay = WechatService::WePayOrder();
        $user = WechatService::instance()->getWebOauthInfo($this->url, 0);
        if (empty($user['openid'])) return '<h3>网页授权获取OPENID失败！</h3>';
        // 生成预支付码
        $result = $this->pay->create([
            'body'             => '测试商品',
            'openid'           => $user['openid'],
            'total_fee'        => '1',
            'trade_type'       => 'JSAPI',
            'notify_url'       => sysuri('wechat/api.test/notify', [], false, true),
            'out_trade_no'     => CodeExtend::uniqidDate(18),
            'spbill_create_ip' => $this->getClientIp(),
        ]);
        // 创建JSAPI参数签名
        $options = $this->pay->jsapiParams($result['prepay_id']);
        // JSSDK 签名配置
        $optionJSON = json_encode($options, JSON_UNESCAPED_UNICODE);
        $configJSON = json_encode(WechatService::instance()->getWebJssdkSign(), JSON_UNESCAPED_UNICODE);

        echo '<pre>';
        echo "当前用户OPENID: {$user['openid']}";
        echo "\n--- 创建预支付码 ---\n";
        var_export($result);
        echo '</pre>';

        echo '<pre>';
        echo "\n\n--- JSAPI 及 H5 参数 ---\n";
        var_export($options);
        echo '</pre>';
        echo "<button id='paytest' type='button'>JSAPI支付测试</button>";
        echo "
        <script src='//res.wx.qq.com/open/js/jweixin-1.2.0.js'></script>
        <script>
            wx.config($configJSON);
            alert($configJSON);
            document.getElementById('paytest').onclick = function(){
                var options = $optionJSON;
                options.success = function(){
                    alert('支付成功');
                }
                alert(options);
                wx.chooseWXPay(options);
            }
        </script>";
    }

    

    /**
     * 创建二维码响应对应
     * @param string $url 二维码内容
     * @return \think\Response
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionFailedException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     * @throws \Endroid\QrCode\Exceptions\ImageTypeInvalidException
     */
    protected function showQrc($url)
    {
        $qrCode = new \Endroid\QrCode\QrCode();
        $qrCode->setText($url)->setSize(300)->setPadding(20)->setImageType('png');
        return response($qrCode->get(), 200, ['Content-Type' => 'image/png']);
    }

}
