<?php
namespace app\wechat\controller\school;

use think\admin\Controller;
use think\admin\extend\CodeExtend;

class School extends Controller
{
	protected $table = 'JkSchool';

	/**
     * 学校管理
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $this->title = '学校列表';
        $this->_query($this->table)->order('sort desc, id desc')->page();
	}
	
	/**
     * 添加学校
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        if ($this->request->isGet()) {
            $this->_applyFormToken();
            $this->_form($this->table, 'form');
        } else {
            $data = $this->request->post();
            $data['url'] = CodeExtend::uniqidNumber(32);
            if ($this->app->db->name($this->table)->insert($data) !== false) {
                $this->success('添加成功！', 'javascript:history.back()');
            } else {
                $this->error('添加失败，请稍候再试！');
            }
        }
    }

    /**
     * 编辑学校
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        $id = $this->request->get('id');
        if (empty($id)) $this->error('参数错误，请稍候再试！');
        if ($this->request->isGet()) {
            $this->_applyFormToken();
            $this->_form($this->table, 'form');
        } else {
            if ($this->app->db->name($this->table)->where([
                'id' => $id
            ])->update($this->request->post()) !== false) {
                $this->success('更新成功！', 'javascript:history.back()');
            } else {
                $this->error('更新失败，请稍候再试！');
            }
        }
	}
	
	/**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
			if (!isset($data['id'])) {
				$data['url'] = CodeExtend::uniqidNumber(32);
			}
        }
    }

    /**
     * 删除学校
     * @auth true
     * @throws \think\db\exception\DbException
     */
    public function remove()
    {
        $this->_applyFormToken();
        $this->_delete($this->table);
    }
}