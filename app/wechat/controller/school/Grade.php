<?php
namespace app\wechat\controller\school;

use think\admin\Controller;
use think\Request;

class Grade extends Controller
{
	protected $table = 'JkGrade';
	
	private $schoolIdSessionKey = 'school_id';

	/**
     * 年级管理
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(Request $request)
    {
		$schoolId = $request->get('id', 0);
		$this->app->session->set($this->schoolIdSessionKey, $schoolId);
        $this->title = '年级列表';
        $this->_query($this->table)->where('school_id', $schoolId)->order('sort desc, id asc')->page();
	}
	
	/**
     * 添加年级
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        $this->_applyFormToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 编辑年级
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        $this->_applyFormToken();
        $this->_form($this->table, 'form');
	}

	/**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
			$data['school_id'] = $this->app->session->get($this->schoolIdSessionKey);
        }
    }

    /**
     * 删除年级
     * @auth true
     * @throws \think\db\exception\DbException
     */
    public function remove()
    {
        $this->_applyFormToken();
        $this->_delete($this->table);
    }
}