<?php
namespace app\wechat\controller\school;

use think\admin\Controller;
use think\Request;

class Classs extends Controller
{
	protected $table = 'JkClass';
	
	private $sessionKey = 'grade_id';

	/**
     * 班级管理
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(Request $request)
    {
		$gradeId = $request->get('id', 0);
		$this->app->session->set($this->sessionKey, $gradeId);
        $this->title = '年级列表';
        $this->_query($this->table)->where('grade_id', $gradeId)->order('sort desc, id asc')->page();
	}
	
	/**
     * 添加班级
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        $this->_applyFormToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 编辑班级
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        $this->_applyFormToken();
        $this->_form($this->table, 'form');
	}

	/**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
			$data['grade_id'] = $this->app->session->get($this->sessionKey);
        }
    }

    /**
     * 删除班级
     * @auth true
     * @throws \think\db\exception\DbException
     */
    public function remove()
    {
        $this->_applyFormToken();
        $this->_delete($this->table);
    }
}