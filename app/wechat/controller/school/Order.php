<?php
namespace app\wechat\controller\school;

use think\admin\Controller;
use think\admin\extend\ExcelExtend;
use think\facade\Db;
use think\Request;

class Order extends Controller
{
	protected $table = 'jk_order';

	/**
	 * 订单列表
	 * @auth true
     * @menu true
	 * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
	 */
	public function index()
	{
		$this->title = '订单列表';
		$this->school = Db::table('jk_school')->order('sort desc,id asc')->select();

		$query = $this->_query($this->table)->alias('o')->field('o.*, s.name as school_name, g.name as grade_name, c.name as class_name, p.name as product_name, p.image as product_image');
		$query = $query->equal('o.school_id#school_id,o.grade_id#grade_id,o.class_id#class_id,o.order_no#order_no,o.pay_status#pay_status');
		$query = $query->like('o.student_name#student_name');
		$query = $query->timeBetween('o.create_time#create_at');
		$query = $query->leftJoin('jk_school s', 'o.school_id = s.id');
		$query = $query->leftJoin('jk_grade g', 'o.grade_id = g.id');
		$query = $query->leftJoin('jk_class c', 'o.class_id = c.id');
		$query = $query->leftJoin('jk_product p', 'o.product_id = p.id');
		$query->order('o.id desc')->page();
	}

	public function _index_page_filter(array &$data)
	{
		foreach ($data as &$vo) {
			if ($vo['pay_status'] == 1) {
				$vo['pay_status'] = '支付成功';
			} else {
				$vo['pay_status'] = '待支付';
			}
		}
	}

	public function export(Request $request)
	{
		$schoolId = $request->get('school_id', 0, 'intval');
		$gradeId = $request->get('grade_id', 0, 'intval');
		$classId = $request->get('class_id', 0, 'intval');
		$payStatus = $request->get('pay_status', 0, 'intval');
		$createAt = $request->get('create_at', '');
		if (!$schoolId) {
			return $this->error('错误的参数');
		}

		// 读取学校
		$schoolResult = Db::table('jk_school')->where('id', $schoolId)->find();
		if (!$schoolResult) {
			return $this->error('没有找到学校');
		}

		$gradeResult = [];
		if ($gradeId > 0) {
			// 读取年级
			$gradeResult = Db::table('jk_grade')->where('id', $gradeId)->find();
			if (!$gradeResult) {
				return $this->error('没有找到年级');
			}
		} else {
			$gradeResult['name'] = '所有年级';
		}

		$classResult = [];
		$classResult['num'] = 0;
		if ($classId > 0) {
			// 读取班级数据
			$classResult = Db::table('jk_class')->where('id', $classId)->find();
			if (!$classResult) {
				return $this->error('没有找到班级');
			}
			$exportName = $schoolResult['name'] . '-' . $gradeResult['name'] . '-' . $classResult['name'] . '.csv';
		} else {
			$gradeClassResult = [];
			if ($gradeId > 0) {
				$gradeClassResult = Db::table('jk_class')->where('grade_id', $gradeId)->select();
			} else {
				$gradeRes = Db::table('jk_grade')->where('school_id', $schoolId)->select();
				if (count($gradeRes) > 0) {
					$gradeIds = [];
					foreach($gradeRes as $item) {
						$gradeIds[] = $item['id'];
					}
					$gradeClassResult = Db::table('jk_class')->where('grade_id', 'in', $gradeIds)->select();
				}
			}
			if (count($gradeClassResult) <= 0) {
				return $this->error('没有找到对应年级的班级列表');
			}
			$classResult = [];
			$classPreson = 0;
			foreach($gradeClassResult as $val) {
				$classPreson += $val['num'];
			}
			$classResult['num'] = $classPreson;
			$exportName = $schoolResult['name'] . '-' . $gradeResult['name'] . '.csv';
		}
		

		
		$lists = [];
		$db = Db::table('jk_order')->alias('o')
		->field('o.*, s.name as school_name, g.name as grade_name, c.name as class_name, p.name as product_name, p.image as product_image')
		->leftJoin('jk_school s', 'o.school_id = s.id')
		->leftJoin('jk_grade g', 'o.grade_id = g.id')
		->leftJoin('jk_class c', 'o.class_id = c.id')
		->leftJoin('jk_product p', 'o.product_id = p.id');

		if ($createAt) {
			$createAt = explode(' - ', $createAt);
			$createAt[0] = strtotime($createAt[0] . ' 00:00:00');
			$createAt[1] = strtotime($createAt[1] . ' 23:59:00');
			$db = $db->whereBetween('o.create_time', $createAt);
		}

		if ($schoolId > 0) {
			$db = $db->where('o.school_id', $schoolId);
		}

		if ($classId > 0) {
			$db = $db->where('o.class_id', $classId);
		}
		if ($gradeId > 0) {
			$db = $db->where('o.grade_id', $gradeId);
		}
		$db = $db->where('o.pay_status', $payStatus)
		->chunk(100, function ($datas) use (&$lists) {
			foreach ($datas as $data) {
				$lists[] = $data;
			}
		}, 'o.id');

		ExcelExtend::header($exportName, [
			'学校', '年级', '班级', '学生姓名', '学生性别', '学生身高', '学生体重', '家长姓名', '联系电话', '订购数量', '支付价格', '备注'
		]);
		ExcelExtend::body($lists, ['order.school_name', 'order.grade_name', 'order.class_name', 'order.student_name', 'order.sex', 'order.height', 'order.weight', 'order.parent_name', 'order.contact_phone', 'order.order_num', 'order.order_price', 'order.remark']);
		$classNum = '';
		$scale = '';
		if ($classResult['num'] > 0) {
			$classNum = '应有人数：' . $classResult['num'];
			$scale = '订购比例：' . number_format((count($lists)  / $classResult['num']) * 100, 2) . '%';
		}
		ExcelExtend::body([
			[
				'total' => '订购人数：' . count($lists),
				'class_num' => $classNum,
				'scale' => $scale,
			]
		], ['order.total', 'order.class_num', 'order.scale']);
	}

	public function getGradeList(Request $request)
	{
		$data = Db::table('jk_grade')->where('school_id', $request->get('id'))->order('sort desc, id asc')->select();
		$this->success('获取成功', $data);
	}

	public function getClassList(Request $request)
	{
		$data = Db::table('jk_class')->where('grade_id', $request->get('id'))->order('sort desc, id asc')->select();
		$this->success('获取成功', $data);
	}

	/**
     * 编辑学校
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(Request $request)
    {
		$this->_applyFormToken();
        $this->_form($this->table, 'detail');
	}

	/**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if (!$this->request->isPost()) {
			$this->title = '订单 '. $data['order_no'] .' 详情';
			$this->school = Db::table('jk_school')->where('id', $data['school_id'])->find();
			$this->grade = Db::table('jk_grade')->where('id', $data['grade_id'])->find();
			$this->classs = Db::table('jk_class')->where('id', $data['class_id'])->find();
			$this->product = Db::table('jk_product')->where('id', $data['product_id'])->find();
			if ($data['pay_status'] == 1) {
				$data['pay_status'] = '支付成功';
			} else {
				$data['pay_status'] = '待支付';
			}
        }
    }
}