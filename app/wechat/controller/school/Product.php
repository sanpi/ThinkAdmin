<?php
namespace app\wechat\controller\school;

use think\admin\Controller;
use think\admin\extend\CodeExtend;
use think\Request;

class Product extends Controller
{
	protected $table = 'JkProduct';
	
	private $sessionKey = 'school_id';

	/**
     * 班级管理
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(Request $request)
    {
		$schoolId = $request->get('id', 0);
		$this->app->session->set($this->sessionKey, $schoolId);
        $this->title = '校服列表';
        $this->_query($this->table)->where('school_id', $schoolId)->order('sort desc, id desc')->page();
	}
	
	/**
     * 添加班级
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        $this->_applyFormToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 编辑班级
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        $this->_applyFormToken();
        $this->_form($this->table, 'form');
	}

	/**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            if (!isset($data['id'])) {
				$data['url'] = CodeExtend::uniqidNumber(32);
			}
			$data['school_id'] = $this->app->session->get($this->sessionKey);
        }
    }

    /**
     * 删除班级
     * @auth true
     * @throws \think\db\exception\DbException
     */
    public function remove()
    {
        $this->_applyFormToken();
        $this->_delete($this->table);
    }
}